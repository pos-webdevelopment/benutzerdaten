<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Benutzerdaten - Details</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script>
        function goback() {
            history.go(-1);
        }
    </script>
</head>
<body>
<?php
require "func.inc.php";
?>
<div class="container">
    <h1 class="mt-5 mb-3">Benutzerdaten anzeigen</h1>
    <a href="javascript:goback()">Zurück</a>
    <br><br>
    <div>
        <?php
        if ($connect->connect_errno == 0) {
            getDataPerId($_GET['id']);
        } else {
            echo $connect->connect_error;
        }
        ?>
    </div>
</div>
</body>
</html>