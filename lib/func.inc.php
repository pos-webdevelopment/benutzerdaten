<?php
$host = 'localhost';
$username = 'root';
$passwd = '';
$dbname = 'php13';
$connect = new mysqli($host, $username, $passwd, $dbname);
$search = '';

function getFilteredData($filter) {
    global $connect;
    $matches = true;
    $query = "SELECT * FROM user WHERE firstname LIKE '%$filter%' || lastname LIKE '%$filter%' || concat(firstname, ' ', lastname) LIKE '%$filter%'|| email LIKE '%$filter%' || street LIKE '%$filter%';";
    $result = $connect->query($query);
    $rows = mysqli_num_rows($result);
    if($rows > 1) {
        echo '<p class="alert alert-success">' . $rows . ' Einträge gefunden.</p>';
        //echo $rows . ' entries found.';
    } elseif($rows == 0) {
        echo '<p class="alert alert-danger">Es konnten keine Daten passend zu den Suchkriterien gefunden werden.</p>';
        $matches = false;
    } else {
        echo '<p class="alert alert-success">' . $rows . ' Eintrag gefunden.</p>';
    }
    if ($result == false) {
        echo $connect->error;
    } elseif ($matches) {
        echo '<table class="table table-striped"><tr><th>Name</th><th>E-Mail</th><th>Geburtsdatum</th></tr>';
        while ($row = $result->fetch_object()) {
            $u = "lib/details.php?id=";
            echo '<tr><td><a href="'.$u.$row->id.'">' . $row->firstname . ' ' . $row->lastname . '</a></td>';
            echo '<td>' . $row->email . '</td>';
            echo '<td>' . date("d.m.Y", strtotime($row->birthdate)) . '</td></tr>';
        }
    }
}

function getDataPerId($id) {
    global $connect;
    $query = "SELECT * FROM user WHERE id = '$id'";
    $result = $connect->query($query);
    if ($result == false) {
        echo $connect->error;
    } else {
        $row = $result->fetch_object();
        echo '<table class="table table-responsive-sm table-striped">';
        echo '<tr><th>Vorname</th><td>'.$row->firstname.'</td></tr>';
        echo '<tr><th>Nachname</th><td>'.$row->lastname.'</td></tr>';
        echo '<tr><th>Geburtsdatum</th><td>'. date("d.m.Y", strtotime($row->birthdate)) .'</td></tr>';
        echo '<tr><th>E-Mail</th><td>'.$row->email.'</td></tr>';
        echo '<tr><th>Telefon</th><td>'.$row->phone.'</td></tr>';
        echo '<tr><th>Straße</th><td>'.$row->street.'</td></tr>';
        echo '</table>';
    }
}
?>