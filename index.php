<html>
<head>
    <meta charset="UTF-8">
    <title>Benutzerdaten suchen</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<?php
$search = '';
isset($_GET['search']) ? $search = $_GET['search'] : ' ';
?>
<div class="container">
    <h1 class="mt-5 mb-3">Benutzerdaten anzeigen</h1>
    <form method="get" action="index.php" id="searchForm">
        <div class="form-check">
            <label for="id">Suche: </label>
            <input id="id" name="search" class="col-sm-3" value="<?= htmlspecialchars($search) ?>"/>
            <input type="submit" class="btn btn-primary" value="Suchen"/>
            <a href="index.php" class="btn btn-secondary">Leeren</a>
        </div>
    </form>
    <div>
        <?php
        require "lib/func.inc.php";
        $s = '';
        if ($connect->connect_errno == 0) {
            //echo 'Connection stable';
            if (!isset($_GET['search'])) {
                $_GET['search'] = '';
            } else {
                $s = $_GET['search'];
            }
            getFilteredData($s);
        } else {
            echo $connect->connect_error;
        }
        ?>
    </div>
</div>
</body>
</html>